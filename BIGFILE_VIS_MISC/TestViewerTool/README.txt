
[NOTE]
This is simplified code just to get data from bigfile dataset, so can be used as Visit plugin code. Refered from bigfile-cat.

[BUILD]
$ mkdir /tmp/BIGFILE;  cd /tmp/BIGFILE
$ git clone https://github.com/rainwoodman/bigfile
$ cd bigfile/src
$ make
$ gcc  -I/tmp/BIGFILE/bigfile/src  -L/tmp/BIGFILE/bigfile/src -lbigfile  -o  readBigfileSimple readBigfileSimple.c
"
