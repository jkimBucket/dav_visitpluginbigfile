#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <stddef.h>

#include <unistd.h>
#include <libgen.h>
#include <signal.h>
#include "bigfile.h"


static void usage() {
    fprintf(stderr, "Command Options:  [-o offset] [-c count] <filepath-to-block> \n");
    fprintf(stderr, "-o seek to item at offset \n");
    fprintf(stderr, "-c read item count \n");
    exit(1);
}

int main(int argc, char * argv[]) {
    BigFile bf = {0};
    BigBlock bb = {0};
    int opt;

    ptrdiff_t offset = 0;
    ptrdiff_t count = -1;

    size_t buffersize = 32 * 1024 * 1024; // 32GB
    //size_t buffersize = 1024 * 1024;

    while(-1 != (opt = getopt(argc, argv, "o:c:"))) {
        switch(opt) {
            case 'o':
                sscanf(optarg, "%td", &offset);
                break;
            case 'c':
                sscanf(optarg, "%td", &count);
                break;
            default:
                usage();
        }
    }

    if(argc - optind != 1) {
        usage();
    }
    argv += optind - 1;



#if 1  // combined path
    #if 0
    for (int i=0; i<argc; i++)
        printf("JKDBG> argv[%d]: %s\n", i, argv[i]);
    printf("JKDBG> pathStr:%s , blockStr:%s \n", dirname(argv[1]), basename(argv[1]));
    #endif

    if(0 != big_file_open(&bf, dirname(argv[1]))) {
        fprintf(stderr, "failed to open: %s: %s\n", dirname(argv[1]), big_file_get_error_message());
        exit(1);
    }
    if(0 != big_file_open_block(&bf, &bb, basename(argv[1]))) {
        fprintf(stderr, "failed to open: %s: %s\n", basename(argv[1]), big_file_get_error_message());
        exit(1);
    }
#else // seperated path
    if(0 != big_file_open(&bf, argv[1])) {
        fprintf(stderr, "failed to open: %s: %s\n", argv[1], big_file_get_error_message());
        exit(1);
    }
    if(0 != big_file_open_block(&bf, &bb, argv[2])) {
        fprintf(stderr, "failed to open: %s: %s\n", argv[2], big_file_get_error_message());
        exit(1);
    }
#endif


    if(count == -1 || bb.size < count + offset) {
        count = bb.size - offset;
    } 


    ptrdiff_t endPos = offset + count;

    size_t chunksize = buffersize / (bb.nmemb * dtype_itemsize(bb.dtype));
    ptrdiff_t beginPos;
    BigArray array;

    for(beginPos = offset; beginPos < endPos; ) {
        if(chunksize > count) chunksize = count;
        if(0 != big_block_read_simple(&bb, beginPos, chunksize, &array, NULL)) {
            fprintf(stderr, "failed to read original: %s\n", big_file_get_error_message());
            exit(1);
        }

        char str[300];
        BigArrayIter iter;
        big_array_iter_init(&iter, &array);
        size_t idx=0;
        size_t i;
        int numBytesPerDatatype = dtype_itemsize(array.dtype);
        for(i = 0; i < array.dims[0]; i++) {
            int j;
            for(j = 0; j < bb.nmemb; j ++) {
                dtype_format(str, array.dtype, iter.dataptr, NULL);
                big_array_iter_advance(&iter);

                #if 1 // as a value directly from data buffer // JKTEST
                if(numBytesPerDatatype == 4) // if 4 byte
                    printf("%5.4f  ", ((float*)array.data)[idx++]);
                else if(numBytesPerDatatype == 8) // if  8 byte
                    printf("%5.4f  ", ((double*)array.data)[idx++]);
                #else  // as string
                printf("%s ", str);
                #endif
            }
            fprintf(stdout, "\n");
        }
        free(array.data);
        beginPos += array.dims[0];
    }

    big_block_close(&bb);
    big_file_close(&bf);
    return 0;
}
