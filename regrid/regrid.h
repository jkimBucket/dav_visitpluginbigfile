#include <vector>
#include <map>
#include <string>
#include <cstring>
#include <iterator>
#include <iostream>
#include <sstream>
#include <fstream>
#include <algorithm>

#include <stdio.h>
#include <stdlib.h>

#include <bigfile.h>

#ifdef PARALLEL
#include <mpi.h>
#endif

using namespace std;

int nBlocks;
int nFiles;

int myRank, ranks;

BigFile bf = {0};
BigBlock bb = {0};

vector<int> directories;
vector<int> subDomains;
vector<int> fileCounts;
vector<size_t>fileOffsets;

vector<double> fullX;
vector<double> fullY;
vector<double> fullZ;
vector<double> subDims;

vector<map<size_t, size_t> > dOffsets;

string dataFilePath;

char pGrid[1024];
char offsetDir[1024];

void    readInfoFile(char *);
void    readPositionHeader(int);
void    readDataFile(ptrdiff_t, ptrdiff_t);
void    writeDataFiles(int);
