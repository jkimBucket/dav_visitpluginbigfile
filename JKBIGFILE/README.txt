

==========================
[BUILD]
- This JKBIGFILE can be cloned at ".../VisIT/Source/visit2.12.3/src/databases/" as other plugins.

- Notice that I need to add bigfile.a to CMakeLists.txt file.
- The bigfile header (bigfile.h) and library (libbigfile.a) is in bigfile dir. However, I had to copy the bigfile library into ".../VisIT/Source/visit2.12.3/src/lib/libbigfile.a" (Visit source dir) to be able to build 

- The rest is the same as building a Visit plugin 


=========================
[OPEN plugin file]

- file extention is *.jkbigfile 

- DataTest/ dir contains this plugin's  header fil (bigfile_pointMesh.jkbigfile) and bigfile data "PART_000/"

- bigfile/ contains the bigfile source, which the xml file now correctly points to.

- all auto generated files are no longer tracked, so the correct way to build is now:

clone
cd bigfile/src; make

xmledit jkbigfile.xml
 >> generate plugin information, attributes, and cmake files

cmake .
make
